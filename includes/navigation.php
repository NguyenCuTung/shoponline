<?php
$sql = "select * from categories where parent=0";
$pquery = $db->query($sql);
?>
<nav class="navbar navbar-defauld">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="index.php" class="navbar-brand">Shop Online</a>
            <ul class="nav navbar-nav ">
                <?php
                while ($parent = mysqli_fetch_assoc($pquery)):
                    $parent_id = $parent['id'];
                    $sql2 = "select * from categories where parent = '$parent_id'";
                    $cquery = $db->query($sql2);
                    ?>
                    <!--                    Menu -->
                    <li class="nav navbar-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $parent['category']; ?>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <?php while ($child = mysqli_fetch_assoc($cquery)) : ?>
                                <li>
                                    <a href="category.php?cat=<?= $child['id']; ?>"><?php echo $child['category']; ?></a>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </li>
                <?php endwhile; ?>
                <li><a href="cart.php"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
            </ul>
        </div>
    </div>
</nav>