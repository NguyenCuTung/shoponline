<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/shoponline/core/init.php';
$name = sanitize($_POST['full_name']);
$email = sanitize($_POST['email']);
$street = sanitize($_POST['street']);
$street2 = sanitize($_POST['street2']);
$city = sanitize($_POST['city']);
$state = sanitize($_POST['state']);
$zip_code = sanitize($_POST['zip_code']);
$country = sanitize($_POST['country']);
$errors = array();
$required = array(
    'Full Name'      => $name,
    'Email'          => $email,
    'Street'         => $street,
    'Street 2'        => $street2,
    'City'           => $city,
    'State'          => $state,
    'Zip Code'       => $zip_code,
    'Country'        => $country,
);

foreach ($required as $f => $d) {
    if (empty($d) || $d ==''){
        $errors[] = $f.' is required.' ;
    }
}
if (!filter_var($email,FILTER_VALIDATE_EMAIL)){
    $errors[]='Please enter a valid email.';
}
if (!empty($errors)){
    echo display_errors($errors);
}else{
    echo 'passed';
}
?>